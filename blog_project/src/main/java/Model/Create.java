package Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "relation")
public class Create implements Serializable {
    @Id
    private Integer id_article;
    @Id
    private Integer id_member;

    public Create(){ }

    public Create(Integer id_member, Integer id_article){
        this.id_member = id_member;
        this.id_article = id_article;
    }

    public Integer getId_article() {
        return id_article;
    }

    public void setId_article(Integer id_article) {
        this.id_article = id_article;
    }

    public Integer getId_member() {
        return id_member;
    }

    public void setId_member(Integer id_member) {
        this.id_member = id_member;
    }
}
