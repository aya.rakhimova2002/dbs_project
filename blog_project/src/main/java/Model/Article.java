package Model;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "articles")
public class Article {
    @Id
    private int id;

    private String title;
    private String date;
    private String text;

    @ManyToMany
    @JoinTable(name = "relation", joinColumns = @JoinColumn(name = "id_article"), inverseJoinColumns = @JoinColumn(name = "id_member"))
    private Collection<Member> authors;


    public Article(){ }

    public Article(int id, String title, String text, String date) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.date = date;
    }

    public int getId(){ return id; }
    public void setId(int id) { this.id = id; }

    public String getTitle(){ return title; }
    public void setTitle(String title){ this.title = title; }

    public String getDate() { return date; }
    public void setDate(String date) { this.date = date; }

    public String getText() { return text; }
    public void setText(String text) { this.text = text; }

    public Collection<Member> getAuthor() { return authors; }
    public void setAuthor(List<Member> author) { this.authors = author; }
}
