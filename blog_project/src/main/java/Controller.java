import Service.*;
import Info.*;
import Model.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

import javax.swing.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    ArticleService articleService = new ArticleService();
    MemberService memberService = new MemberService();
    CreateService createService = new CreateService();

//Main panel
    @FXML
    private Pane main;
    @FXML
    private Button view_member;
    @FXML
    private Button view_article;
    @FXML
    private Button view_create;


// Show the Member window
    @FXML
    private Pane member_form;
    @FXML
    private ScrollPane member_table_fragment;

    @FXML
    private void showMemberTable(ActionEvent event){
        clean();

        member_form.setVisible(true);
        member_table_fragment.setVisible(true);
    }
//    Member window

//    Left panel
    @FXML
    private Label member_lable;
    @FXML
    private TextField newMember_id;
    @FXML
    private TextField newMember_name;
    @FXML
    private TextField newMember_surname;
    @FXML
    private TextField newMember_email;
    @FXML
    private TextField newMember_phone;
    @FXML
    private TextField newMember_role;
    @FXML
    private Button add_newMember;
    @FXML
    private Button back_member;
    @FXML
    private Button ok_update_member;
    @FXML
    private Button cancel_update_member;

//    Operations
    @FXML
    private Button delete_member;
    @FXML
    private Button update_member;
    @FXML
    private Button load_data_member;

//    Table
    @FXML
    private TableView<MemberInfo> member_table;
    @FXML
    private TableColumn<MemberInfo, Integer> id_column;
    @FXML
    private TableColumn<MemberInfo, String> name_column;
    @FXML
    private TableColumn<MemberInfo, String> surname_column;
    @FXML
    private TableColumn<MemberInfo, String> email_column;
    @FXML
    private TableColumn<MemberInfo, String> phone_column;
    @FXML
    private TableColumn<MemberInfo, String> role_column;

    Pattern rightPhoneNumber = Pattern.compile("^((\\+?)([0-9]{1,5})([0-9]{6,11}))$");
    Pattern rightString = Pattern.compile("[a-zA-Z]+");
    Pattern rightEmail = Pattern.compile("^((\\w|[+-])+(\\.[\\w-]+)*@[\\w-]+((\\.[\\d\\p{Alpha}]+)*(\\.\\p{Alpha}{2,})*)*)$");
    Pattern rightId = Pattern.compile("[0-9]+");

    //check right format for name, surname, telephone number and role
    public boolean validationMember(Member member){
        Matcher matchName = rightString.matcher(member.getName());
        Matcher matchSurname = rightString.matcher(member.getSurname());
        Matcher matchPhone = rightPhoneNumber.matcher(member.getPhone());
        Matcher matchRole = rightString.matcher(member.getRole());
        if(memberService.isMemberExist(member.getId())) {
            if (checkEmail(member.getEmail())) {
                if (matchName.matches() && matchSurname.matches() &&
                        matchPhone.matches() && matchRole.matches()) {
                    return true;
                } else
                return false;
            }else
            return false;
        }else
        return false;
    }


    //check if the phone number is in the correct format
    public boolean checkPhone(String phone) {
        Matcher matchPhone = rightPhoneNumber.matcher(phone);
        if (matchPhone.matches()) {
            return true;
        } else {

            return false;
        }
    }

    //check email format
    public boolean checkEmail(String email){
        Pattern rightEmail = Pattern.compile("^((\\w|[+-])+(\\.[\\w-]+)*@[\\w-]+((\\.[\\d\\p{Alpha}]+)*(\\.\\p{Alpha}{2,})*)*)$");
        Matcher matchEmail = rightEmail.matcher(email);
        if(matchEmail.matches()){
//            if(uniqueEmail(email)) return true;
//            else {
//                return false;
//            }
            return true;
        }else
            return false;
//        return matchEmail.matches();
    }

    public boolean uniqueEmail(String email){
        List<Member> emails = memberService.getAllMembers();
        for (Member e : emails) if (e.getEmail().equals(email)) return false;
        return true;
    }

    //check if text fields contain only letters
    public boolean checkString(String str) {
        Matcher matchStr = rightString.matcher(str);
        if (matchStr.matches()) {
            return true;
        } else {
            errorMessage("Name field is not filled in according to the format");
            return false;
        }
    }


    //check if text fields are empty or not
    public boolean isEmpty(String[] tFields) {
        for (String s : tFields) {
            if (s.length() == 0) {
                errorMessage("One or several fields are empty");
                return false;
            }
        }
        return true;
    }

    public boolean validationId(String id) {
        Matcher matchId = rightId.matcher(id);
        if (matchId.matches()) {
            return true;
        } else {
            errorMessage("ID can contain only numbers");
            return false;
        }
    }



//  Create new Member
    @FXML
    public  void newMember(ActionEvent event){
        String[] dataFromTFields = new String[] {
                newMember_id.getText(),
                newMember_name.getText(),
                newMember_surname.getText(),
                newMember_email.getText(),
                newMember_phone.getText(),
                newMember_role.getText(),
        };

        if(isEmpty(dataFromTFields)){
            Member member = new Member(Integer.parseInt(newMember_id.getText()), newMember_name.getText(),
                    newMember_surname.getText(), newMember_email.getText(), newMember_phone.getText(), newMember_role.getText());
            addNewMemberToDB(member);
        }
    }

    //add new member to database if text fields are filled in correctly
    public Member addNewMemberToDB(Member member) {
        if(memberService.findMember(Integer.parseInt(newMember_id.getText())) == null){
            validationMember(member);
            if (validationId(Integer.toString(member.getId())) && checkString(member.getName()) && checkString(member.getSurname()) && checkPhone(member.getPhone()) && checkString(member.getRole())) {
                memberService.saveMember(member);
                newMember_id.setText("");
                newMember_name.setText("");
                newMember_surname.setText("");
                newMember_email.setText("");
                newMember_phone.setText("");
                newMember_role.setText("");
                loadDataMember();
                return member;
            }
        } else {
            errorMessage("Member with the same ID already exists");
        }
        return null;
    }

//  Go back to Main
    @FXML
    public void goBackFromMember(ActionEvent event){
        main.setVisible(true);
        view_create.setVisible(true);
        view_member.setVisible(true);
        view_article.setVisible(true);

        member_form.setVisible(false);
        member_table_fragment.setVisible(false);
    }

//  Selected row
    private MemberInfo selectionMemberInfo = null;

//  Delete Member
    @FXML
    public void deleteMember(ActionEvent event){
        if(selectionMemberInfo != null){
            Member member = new Member(selectionMemberInfo.getNewMember_id(), selectionMemberInfo.getNewMember_name(),
                    selectionMemberInfo.getNewMember_surname(), selectionMemberInfo.getNewMember_email(),
                    selectionMemberInfo.getNewMember_phone(), selectionMemberInfo.getNewMember_role());
            List<Create> relation = createService.getAllRelations();
            for (Create c: relation){
                if(c.getId_member() == selectionMemberInfo.getNewMember_id()){
                    errorMessage("It is not possible to delete this object, because it is related to Article in another table!");
                    return;
                }
            }
            memberService.deleteMember(member);
            loadDataMember();
            selectionMemberInfo = null;
        }else{
            errorMessage("Click on the row you want to delete!");
        }
    }

//  Update Member window
    @FXML
    public void updateMember(ActionEvent event){
        if(selectionMemberInfo != null){
            newMember_id.setText(Integer.toString(selectionMemberInfo.getNewMember_id()));
            newMember_name.setText(selectionMemberInfo.getNewMember_name());
            newMember_surname.setText(selectionMemberInfo.getNewMember_surname());
            newMember_email.setText(selectionMemberInfo.getNewMember_email());
            newMember_phone.setText(selectionMemberInfo.getNewMember_phone());
            newMember_role.setText(selectionMemberInfo.getNewMember_role());

            member_lable.setText("Update Member");
            add_newMember.setVisible(false);
            ok_update_member.setVisible(true);
            cancel_update_member.setVisible(true);
            update_member.setVisible(false);
            delete_member.setVisible(false);
            load_data_member.setVisible(false);
            back_member.setVisible(false);

        }else{
            errorMessage("Click on the row you want to update!");
        }
    }

//  Click on OK in Update window
    @FXML
    public void clickOkUpdateMember(ActionEvent event){
        String[] dataFromTFields = new String[] {
                newMember_id.getText(),
                newMember_name.getText(),
                newMember_surname.getText(),
                newMember_email.getText(),
                newMember_phone.getText(),
                newMember_role.getText(),
        };

        if(selectionMemberInfo.getNewMember_id() == Integer.parseInt(newMember_id.getText()) && isEmpty(dataFromTFields)){
            Member member = new Member(Integer.parseInt(newMember_id.getText()), newMember_name.getText(),
                    newMember_surname.getText(), newMember_email.getText(), newMember_phone.getText(), newMember_role.getText());

            if (checkString(member.getName()) && checkString(member.getSurname()) && checkPhone(member.getPhone()) && checkString(member.getRole())) {
                memberService.updateMember(member);
                newMember_id.setText("");
                newMember_name.setText("");
                newMember_surname.setText("");
                newMember_email.setText("");
                newMember_phone.setText("");
                newMember_role.setText("");
                loadDataMember();

                selectionMemberInfo = null;
                member_lable.setText("New Member");
                add_newMember.setVisible(true);
                ok_update_member.setVisible(false);
                cancel_update_member.setVisible(false);
                update_member.setVisible(true);
                delete_member.setVisible(true);
                load_data_member.setVisible(true);
                back_member.setVisible(true);
            } else {
                errorMessage("Check the form and try again!");
            }
        }else{
            errorMessage("The Id is not subject to change!");
        }

    }

//  Click on Cancel in Update window
    @FXML
    public void clickCancelUpdateMember(ActionEvent event){
        newMember_id.setText("");
        newMember_name.setText("");
        newMember_surname.setText("");
        newMember_email.setText("");
        newMember_phone.setText("");
        newMember_role.setText("");
        selectionMemberInfo = null;
        loadDataMember();

        member_lable.setText("New Member");
        add_newMember.setVisible(true);
        ok_update_member.setVisible(false);
        cancel_update_member.setVisible(false);
        update_member.setVisible(true);
        delete_member.setVisible(true);
        load_data_member.setVisible(true);
        back_member.setVisible(true);
    }

    @FXML
    public void loading(ActionEvent event){
        loadDataMember();
    }

//  Add info from db to table
    public void loadDataMember(){
        TableView.TableViewSelectionModel<MemberInfo> selectionModel = member_table.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<MemberInfo>(){
            public void changed(ObservableValue<? extends MemberInfo> observable, MemberInfo oldValue, MemberInfo newValue) {
                if (newValue != null) selectionMemberInfo = newValue;
            }
        });

        ObservableList<MemberInfo> data = FXCollections.observableArrayList();

        List<Member> list = memberService.getAllMembers();

        for (Member m : list){
            data.add(new MemberInfo(m.getId(), m.getName(), m.getSurname(), m.getEmail(), m.getPhone(), m.getRole()));
        }

        this.id_column.setCellValueFactory(new PropertyValueFactory<MemberInfo, Integer>("newMember_id"));
        this.name_column.setCellValueFactory(new PropertyValueFactory<MemberInfo, String>("newMember_name"));
        this.surname_column.setCellValueFactory(new PropertyValueFactory<MemberInfo, String>("newMember_surname"));
        this.email_column.setCellValueFactory(new PropertyValueFactory<MemberInfo, String>("newMember_email"));
        this.phone_column.setCellValueFactory(new PropertyValueFactory<MemberInfo, String>("newMember_phone"));
        this.role_column.setCellValueFactory(new PropertyValueFactory<MemberInfo, String>("newMember_role"));
        this.member_table.setItems(data);
    }



//  Show the Article window
    @FXML
    private Pane article_form;
    @FXML
    private ScrollPane article_table_fragment;

    @FXML
    private void showArticleTable(ActionEvent event){
        clean();

        article_form.setVisible(true);
        article_table_fragment.setVisible(true);
    }

//  Article window
//  Left panel
    @FXML
    private Label article_lable;
    @FXML
    private TextField newArticle_id;
    @FXML
    private TextField newArticle_title;
    @FXML
    private TextField newArticle_text;
    @FXML
    private TextField newArticle_date;
    @FXML
    private Button add_newArticle;
    @FXML
    private Button back_article;
    @FXML
    private Button ok_update_article;
    @FXML
    private Button cancel_update_article;

//  Operations
    @FXML
    private Button delete_article;
    @FXML
    private Button update_article;
    @FXML
    private Button load_data_article;

//  Table
    @FXML
    private TableView<ArticleInfo> article_table;
    @FXML
    private TableColumn<ArticleInfo, Integer> id_article_column;
    @FXML
    private TableColumn<ArticleInfo, String> title_column;
    @FXML
    private TableColumn<ArticleInfo, String> text_column;
    @FXML
    private TableColumn<ArticleInfo, String> date_column;

    Pattern rightDate = Pattern.compile("(0?[1-9]|[12][1-9]|3[01])-(0?[1-9]|1[012])-(\\d{4})");

    public boolean checkDate(Article article) {
        Matcher matchDate = rightDate.matcher(article.getDate());
        return matchDate.matches();
    }

//  Create new Article
    @FXML
    public  void newArticle(ActionEvent event){
        String[] dataFromTFields = new String[] {
                newArticle_id.getText(),
                newArticle_title.getText(),
                newArticle_text.getText(),
                newArticle_date.getText(),
        };

        if(isEmpty(dataFromTFields)){
            Article article = new Article(Integer.parseInt(newArticle_id.getText()), newArticle_title.getText(),
                    newArticle_text.getText(), newArticle_date.getText());
            addNewArticleToDB(article);
            checkDate(article);
        }else{
            errorMessage("Check the form and try again!");
        }
    }

    public Article addNewArticleToDB(Article article) {
        if (validationId(Integer.toString(article.getId())) && checkDate(article)) {
            if (articleService.findArticle(Integer.parseInt(newArticle_id.getText())) == null) {
                articleService.saveArticle(article);
                newArticle_id.setText("");
                newArticle_title.setText("");
                newArticle_text.setText("");
                newArticle_date.setText("");
                loadDataArticle();
                return article;
            } else {
                errorMessage("Article with the same ID already exists");
            }
        }
        return null;
    }

//  Go back to main
    @FXML
    public void goBackFromArticle(ActionEvent event){
        main.setVisible(true);
        view_create.setVisible(true);
        view_member.setVisible(true);
        view_article.setVisible(true);

        article_form.setVisible(false);
        article_table_fragment.setVisible(false);
    }

//  Selected row
    private ArticleInfo selectionArticleInfo = null;

//  Delete Article
    @FXML
    public void deleteArticle(ActionEvent event){
        if(selectionArticleInfo != null){
            Article article = new Article(selectionArticleInfo.getNewArticle_id(), selectionArticleInfo.getNewArticle_title(),
                    selectionArticleInfo.getNewArticle_text(), selectionArticleInfo.getNewArticle_date());
            List<Create> relation = createService.getAllRelations();
            for (Create c: relation){
                if(c.getId_article() == selectionArticleInfo.getNewArticle_id()){
                    errorMessage("It is not possible to delete this object, because it is related to Member in another table!");
                    return;
                }
            }
            articleService.deleteArticle(article);
            loadDataArticle();
            selectionArticleInfo = null;
        }else{
            errorMessage("Click on the row you want to delete!");

        }
    }

//  Update Article window
    @FXML
    public void updateArticle(ActionEvent event){
        if(selectionArticleInfo != null){
            newArticle_id.setText(Integer.toString(selectionArticleInfo.getNewArticle_id()));
            newArticle_title.setText(selectionArticleInfo.getNewArticle_title());
            newArticle_text.setText(selectionArticleInfo.getNewArticle_text());
            newArticle_date.setText(selectionArticleInfo.getNewArticle_date());

            article_lable.setText("Update Article");
            add_newArticle.setVisible(false);
            ok_update_article.setVisible(true);
            cancel_update_article.setVisible(true);
            update_article.setVisible(false);
            delete_article.setVisible(false);
            load_data_article.setVisible(false);
            back_article.setVisible(false);

        }else{
            errorMessage("Click on the row you want to update!");
        }
    }

//  Click on OK in Update window
    @FXML
    public void clickOkUpdateArticle(ActionEvent event){
        Article article = new Article(Integer.parseInt(newArticle_id.getText()), newArticle_title.getText(),
                newArticle_text.getText(), newArticle_date.getText());

        if(selectionArticleInfo.getNewArticle_id() != Integer.parseInt(newArticle_id.getText())){
            errorMessage("The Id is not subject to change!");
        }else {
            if ((articleService.findArticle(Integer.parseInt(newArticle_id.getText())).getId() == Integer.parseInt(newArticle_id.getText()) &&
                    newArticle_title.getText().length() > 0 && newArticle_text.getText().length() > 0 &&
                    newArticle_date.getText().length() > 0)) {

                articleService.updateArticle(article);
                newArticle_id.setText("");
                newArticle_title.setText("");
                newArticle_text.setText("");
                newArticle_date.setText("");

                loadDataArticle();

                selectionArticleInfo = null;
                article_lable.setText("New Article");
                add_newArticle.setVisible(true);
                ok_update_article.setVisible(false);
                cancel_update_article.setVisible(false);
                update_article.setVisible(true);
                delete_article.setVisible(true);
                load_data_article.setVisible(true);
                back_article.setVisible(true);
            } else {
                errorMessage("Check the form and try again!");
            }
        }
    }

//  Click on Cancel in Update window
    @FXML
    public void clickCancelUpdateArticle(ActionEvent event){
        newArticle_id.setText("");
        newArticle_title.setText("");
        newArticle_text.setText("");
        newArticle_date.setText("");
        selectionArticleInfo = null;
        loadDataArticle();

        article_lable.setText("New Article");
        add_newArticle.setVisible(true);
        ok_update_article.setVisible(false);
        cancel_update_article.setVisible(false);
        update_article.setVisible(true);
        delete_article.setVisible(true);
        load_data_article.setVisible(true);
        back_article.setVisible(true);
    }

    @FXML
    public void loadingArticle(ActionEvent event){
        loadDataArticle();
    }

//    Add info from db to table
    public void loadDataArticle(){
        TableView.TableViewSelectionModel<ArticleInfo> selectionModel = article_table.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<ArticleInfo>(){
            public void changed(ObservableValue<? extends ArticleInfo> observable, ArticleInfo oldValue, ArticleInfo newValue) {
                if (newValue != null) selectionArticleInfo = newValue;
            }
        });

        ObservableList<ArticleInfo> data = FXCollections.observableArrayList();

        List<Article> list = articleService.getAllArticles();

        for (Article l : list){
            data.add(new ArticleInfo(l.getId(), l.getTitle(), l.getText(), l.getDate()));
        }

        this.id_article_column.setCellValueFactory(new PropertyValueFactory<ArticleInfo, Integer>("newArticle_id"));
        this.title_column.setCellValueFactory(new PropertyValueFactory<ArticleInfo, String>("newArticle_title"));
        this.text_column.setCellValueFactory(new PropertyValueFactory<ArticleInfo, String>("newArticle_text"));
        this.date_column.setCellValueFactory(new PropertyValueFactory<ArticleInfo, String>("newArticle_date"));
        this.article_table.setItems(data);
    }


// Show the Relation window
    @FXML
    private Pane relation_form;
    @FXML
    private ScrollPane relation_table_fragment;

    @FXML
    private void showRelationTable(ActionEvent event){
        clean();

        relation_form.setVisible(true);
        relation_table_fragment.setVisible(true);
    }

//  Relation window
//  Left panel
    @FXML
    private Label relation_lable;
    @FXML
    private TextField id_member;
    @FXML
    private TextField id_article;
    @FXML
    private Button add_newRelation;
    @FXML
    private Button back_relation;
    @FXML
    private Button ok_update_relation;
    @FXML
    private Button cancel_update_relation;

//  Operations
    @FXML
    private Button delete_relation;
    @FXML
    private Button update_relation;
    @FXML
    private Button load_data_relation;

//  Table
    @FXML
    private TableView<RelationInfo> relation_table;
    @FXML
    private TableColumn<RelationInfo, Integer> member_id_column;
    @FXML
    private TableColumn<RelationInfo, Integer> article_id_column;


//  New Relation
    @FXML
    public void newRelation (ActionEvent event){
        String[] dataFromTFields = new String[] {
                id_member.getText(),
                id_article.getText(),
        };

        if(isEmpty(dataFromTFields)){
            Create create = new Create(Integer.parseInt(id_member.getText()), Integer.parseInt(id_article.getText()));
            createService.saveRelation(create);
            addNewRelationToDB(create);
        }else{
            errorMessage("Check the form and try again!");
        }
    }

    public Create addNewRelationToDB (Create create) {
        if (memberService.findMember(Integer.parseInt(id_member.getText())) != null &&
                articleService.findArticle(Integer.parseInt(id_article.getText())) != null) {
            if (createService.findById(Integer.parseInt(id_member.getText())) == null && createService.findById(Integer.parseInt(id_article.getText())) == null)
            if (validationId(Integer.toString(create.getId_member())) && validationId(Integer.toString(create.getId_article()))){
                id_article.setText("");
                id_member.setText("");

                loadDataRelation();
                return create;
            }
        } else {
            errorMessage("Member ID or article ID is not found!");
        }
        return null;
    }

//  Go back to main
    @FXML
    public void goBackFromRelation(ActionEvent event){
        main.setVisible(true);
        view_create.setVisible(true);
        view_member.setVisible(true);
        view_article.setVisible(true);

        relation_form.setVisible(false);
        relation_table_fragment.setVisible(false);
    }
//  Selected row
    private RelationInfo selectionRelationInfo = null;

//  Delete Relation
    @FXML
    public void deleteRelation(ActionEvent event){
        if(selectionRelationInfo != null){
            Create create = new Create(selectionRelationInfo.getMember_id(), selectionRelationInfo.getArticle_id());
            createService.deleteRelation(create);
            loadDataRelation();
            selectionRelationInfo = null;
        }else{
            errorMessage("Click on the row you want to delete!");

        }
    }

//  Update Relation window
    @FXML
    public void updateRelation(ActionEvent event){
        if(selectionRelationInfo != null){
            id_member.setText(Integer.toString(selectionRelationInfo.getMember_id()));
            id_article.setText(Integer.toString(selectionRelationInfo.getArticle_id()));

            relation_lable.setText("Update this Relation");
            add_newRelation.setVisible(false);
            ok_update_relation.setVisible(true);
            cancel_update_relation.setVisible(true);
            update_relation.setVisible(false);
            delete_relation.setVisible(false);
            load_data_relation.setVisible(false);
            back_relation.setVisible(false);

        }else{
            errorMessage("Click on the row you want to update!");
        }
    }

//  Click on OK in Update window
    @FXML
    public void clickOkUpdateRelation(ActionEvent event){
        Create newRelation = new Create(Integer.parseInt(id_member.getText()), Integer.parseInt(id_article.getText()));
        Create oldRelation = new Create(selectionRelationInfo.getArticle_id(), selectionRelationInfo.getMember_id());

        if(memberService.findMember(Integer.parseInt(id_member.getText())) != null
                && articleService.findArticle(Integer.parseInt(id_article.getText())) != null){

            createService.saveRelation(newRelation);
            createService.deleteRelation(oldRelation);
            id_article.setText("");
            id_member.setText("");

            loadDataRelation();

            selectionRelationInfo = null;
            relation_lable.setText("Member create Article");
            add_newRelation.setVisible(true);
            ok_update_relation.setVisible(false);
            cancel_update_relation.setVisible(false);
            update_relation.setVisible(true);
            delete_relation.setVisible(true);
            load_data_relation.setVisible(true);
            back_relation.setVisible(true);
        }else{
            errorMessage("Check the form and try again!");
        }
    }

//  Click on Cancel in Update window
    @FXML
    public void clickCancelUpdateRelation(ActionEvent event){
        id_member.setText("");
        id_article.setText("");
        selectionRelationInfo = null;
        loadDataRelation();

        relation_lable.setText("Member create Article");
        add_newRelation.setVisible(true);
        ok_update_relation.setVisible(false);
        cancel_update_relation.setVisible(false);
        update_relation.setVisible(true);
        delete_relation.setVisible(true);
        load_data_relation.setVisible(true);
        back_relation.setVisible(true);
    }

    @FXML
    public void loadingRelation(ActionEvent event){
        loadDataRelation();
    }

//  Add info from db to table
    public void loadDataRelation(){
        TableView.TableViewSelectionModel<RelationInfo> selectionModel = relation_table.getSelectionModel();
        selectionModel.selectedItemProperty().addListener(new ChangeListener<RelationInfo>(){
            public void changed(ObservableValue<? extends RelationInfo> observable, RelationInfo oldValue, RelationInfo newValue) {
                if (newValue != null) selectionRelationInfo = newValue;
            }
        });

        ObservableList<RelationInfo> data = FXCollections.observableArrayList();

        List<Create> list = createService.getAllRelations();

        for (Create l : list){
            data.add(new RelationInfo(l.getId_article(), l.getId_member()));
        }

        this.member_id_column.setCellValueFactory(new PropertyValueFactory<RelationInfo, Integer>("member_id"));
        this.article_id_column.setCellValueFactory(new PropertyValueFactory<RelationInfo, Integer>("article_id"));
        this.relation_table.setItems(data);
    }



//    Clear panels
    public void clean(){
        member_form.setVisible(false);
        member_table_fragment.setVisible(false);

        article_form.setVisible(false);
        article_table_fragment.setVisible(false);

        relation_form.setVisible(false);
        relation_table_fragment.setVisible(false);
    }

//    Error message
    public void errorMessage(String s){
        JOptionPane.showMessageDialog(new JTable(),s,"ERROR",JOptionPane.ERROR_MESSAGE);
    }
}