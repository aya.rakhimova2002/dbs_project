package Info;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class RelationInfo {
    private final SimpleIntegerProperty article_id;
    private final SimpleIntegerProperty member_id;

    public RelationInfo(int article_id, int member_id) {
        this.article_id = new SimpleIntegerProperty(article_id);
        this.member_id = new SimpleIntegerProperty(member_id);
    }

    public int getArticle_id() {
        return article_id.get();
    }

    public IntegerProperty article_idProperty() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id.set(article_id);
    }

    public int getMember_id() {
        return member_id.get();
    }

    public IntegerProperty member_idProperty() {
        return member_id;
    }

    public void setMember_id(int member_id) {
        this.member_id.set(member_id);
    }
}
