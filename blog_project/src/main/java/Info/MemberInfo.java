package Info;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class MemberInfo {
    private final SimpleIntegerProperty newMember_id;
    private final SimpleStringProperty newMember_name;
    private final SimpleStringProperty newMember_surname;
    private final SimpleStringProperty newMember_email;
    private final SimpleStringProperty newMember_phone;
    private final SimpleStringProperty newMember_role;

    public MemberInfo(int newMember_id, String newMember_name, String newMember_surname
    ,String newMember_email, String newMember_phone, String newMember_role) {
        this.newMember_id = new SimpleIntegerProperty(newMember_id);
        this.newMember_name = new SimpleStringProperty(newMember_name);
        this.newMember_surname = new SimpleStringProperty(newMember_surname);
        this.newMember_email = new SimpleStringProperty(newMember_email);
        this.newMember_phone = new SimpleStringProperty(newMember_phone);
        this.newMember_role = new SimpleStringProperty(newMember_role);
    }

    public int getNewMember_id() { return newMember_id.get(); }
    public SimpleIntegerProperty newMember_idProperty() { return newMember_id; }
    public void setNewMember_id(int newMember_id) { this.newMember_id.set(newMember_id); }

    public String getNewMember_name() { return newMember_name.get(); }
    public SimpleStringProperty newMember_nameProperty() { return newMember_name; }
    public void setNewMember_name(String newMember_name) { this.newMember_name.set(newMember_name); }

    public String getNewMember_surname() { return newMember_surname.get(); }
    public SimpleStringProperty newMember_surnameProperty() { return newMember_surname; }
    public void setNewMember_surname(String newMember_surname) { this.newMember_surname.set(newMember_surname); }

    public String getNewMember_phone() { return newMember_phone.get(); }
    public SimpleStringProperty newMember_phoneProperty() { return newMember_phone; }
    public void setNewMember_phone(String newMember_phone) { this.newMember_phone.set(newMember_phone); }

    public String getNewMember_email() { return newMember_email.get(); }
    public SimpleStringProperty newMember_emailProperty() { return newMember_email; }
    public void setNewMember_email(String newMember_email) { this.newMember_email.set(newMember_email); }

    public String getNewMember_role() { return newMember_role.get(); }
    public SimpleStringProperty newMember_roleProperty() { return newMember_role; }
    public void setNewMember_role(String newMember_role) { this.newMember_role.set(newMember_role); }



}
