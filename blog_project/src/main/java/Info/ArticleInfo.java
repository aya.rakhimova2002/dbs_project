package Info;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ArticleInfo {
    private final SimpleIntegerProperty newArticle_id;
    private final SimpleStringProperty newArticle_title;
    private final SimpleStringProperty newArticle_text;
    private final SimpleStringProperty newArticle_date;

    public ArticleInfo(int newArticle_id, String newArticle_title, String newArticle_text
            ,String newArticle_date) {
        this.newArticle_id = new SimpleIntegerProperty(newArticle_id);
        this.newArticle_title = new SimpleStringProperty(newArticle_title);
        this.newArticle_text = new SimpleStringProperty(newArticle_text);
        this.newArticle_date = new SimpleStringProperty(newArticle_date);
    }

    public int getNewArticle_id() { return newArticle_id.get(); }
    public SimpleIntegerProperty newArticle_idProperty() { return newArticle_id; }
    public void setNewArticle_id(int newArticle_id) { this.newArticle_id.set(newArticle_id); }

    public String getNewArticle_title() { return newArticle_title.get(); }
    public SimpleStringProperty newArticle_titleProperty() { return newArticle_title; }
    public void setNewArticle_title(String newArticle_title) { this.newArticle_title.set(newArticle_title); }

    public String getNewArticle_text() { return newArticle_text.get(); }
    public SimpleStringProperty newArticle_textProperty() { return newArticle_text; }
    public void setNewArticle_text(String newArticle_text) { this.newArticle_text.set(newArticle_text); }

    public String getNewArticle_date() { return newArticle_date.get(); }
    public SimpleStringProperty newArticle_dateProperty() { return newArticle_date; }
    public void setNewArticle_date(String newArticle_date) { this.newArticle_date.set(newArticle_date); }
}
