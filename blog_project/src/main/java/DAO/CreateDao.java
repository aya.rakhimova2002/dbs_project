package DAO;

import Model.Article;
import Model.Create;
import Util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class CreateDao {
    public Create findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Create create = session.get(Create.class, id);
        session.close();
        return create;
        //return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Create.class, id);
    }

    public void save(Create create) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();
        session.save(create);
        tr.commit();
        session.close();
    }

    public void update(Create create) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();
        session.update(create);
        tr.commit();
        session.close();
    }

    public void delete(Create create) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();
        session.delete(create);
        tr.commit();
        session.close();
    }

    public List<Create> findAll() {
        List<Create> list = (List<Create>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From Create").list();
        return list;
    }
}
