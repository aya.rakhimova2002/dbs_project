package DAO;

import Model.Article;
import Util.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ArticleDao {
    public Article findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Article article = session.get(Article.class, id);
        session.close();
        return article;
//        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Article.class, id);
    }

    public void save(Article article) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();
        session.save(article);
        tr.commit();
        session.close();
    }

    public void update(Article article) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();
        session.update(article);
        tr.commit();
        session.close();
    }

    public void delete(Article article) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();
        session.delete(article);
        tr.commit();
        session.close();
    }

    public List<Article> findAll() {
        List<Article> articleList = (List<Article>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From Article").list();
        return articleList;
    }
}
