package DAO;

import Model.Member;
import org.hibernate.Session;
import org.hibernate.Transaction;
import Util.HibernateSessionFactoryUtil;

import java.util.List;

public class MemberDao {
    public Member findById(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Member member = session.get(Member.class, id);
        session.close();
        return member;
        //return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Member.class, id);
    }

    public void save(Member member) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();
        session.save(member);
        tr.commit();
        session.close();
    }

    public void update(Member member) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();
        session.update(member);
        tr.commit();
        session.close();
    }

    public void delete(Member member) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();
        session.delete(member);
        tr.commit();
        session.close();
    }

    public List<Member> findAll() {
        List<Member> memberList = (List<Member>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From Member").list();
        return memberList;
    }

}
