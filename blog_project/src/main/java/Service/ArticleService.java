package Service;

import DAO.ArticleDao;
import Model.Article;
import Model.Member;

import java.util.List;

public class ArticleService {
    private ArticleDao articleDao = new ArticleDao();

    public ArticleService (){ }

    public Article findArticle(int id){
        return articleDao.findById(id);
    }

    public void saveArticle(Article article){
        articleDao.save(article);
    }

    public void deleteArticle(Article article){
        articleDao.delete(article);
    }

    public void updateArticle(Article article){
        articleDao.update(article);
    }

    public List<Article> getAllArticles(){
        return articleDao.findAll();
    }

}
