package Service;

import DAO.MemberDao;
import Model.Member;

import java.util.List;

public class MemberService {

    private MemberDao memberDao = new MemberDao();

    public MemberService (){ }

    public Member findMember(int id){
        return memberDao.findById(id);
    }

    public boolean isMemberExist(int id){
        if (findMember(id) == null){
            return true;
        }else
            return false;
    }

    public void saveMember(Member member){
        memberDao.save(member);
    }

    public void deleteMember(Member member){
        memberDao.delete(member);
    }

    public void updateMember(Member member){
        memberDao.update(member);
    }

    public List<Member> getAllMembers(){
        return memberDao.findAll();
    }

}
