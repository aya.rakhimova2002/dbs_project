package Service;

import DAO.CreateDao;
import Model.Article;
import Model.Member;
import Model.Create;

import java.util.List;

public class CreateService {
    private CreateDao createDao = new CreateDao();

    public CreateService (){ }

    public Create findById(int id){
        return createDao.findById(id);
    }

    public void saveRelation(Create create){
        createDao.save(create);
    }

    public void deleteRelation(Create create){
        createDao.delete(create);
    }

    public void updateRelation(Create create){
        createDao.update(create);
    }

    public List<Create> getAllRelations(){
        return createDao.findAll();
    }

}
