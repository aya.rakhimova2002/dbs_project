import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ControllerIntegrationTest {
    @Test
    public void test_isEmpty() {
        Controller controller = new Controller();
        String[] correctArray = new String[]{
                "4",
                "Anna",
                "Morozova",
                "anna44@gmail.com",
                "420777345287",
                "editor"
        };
        String[] incorrectArray1 = new String[]{
                "4",
                "",
                "",
                "anna44@gmail.com",
                "420777345287",
                "editor"
        };
        String[] incorrectArray2 = new String[]{
                "",
                "Anna",
                "Morozova",
                "anna44@gmail.com",
                "",
                "editor"
        };

        boolean actualCorAr = controller.isEmpty(correctArray);
        boolean actualIncorAr1 = controller.isEmpty(incorrectArray1);
        boolean actualIncorAr2 = controller.isEmpty(incorrectArray2);

        assertTrue(actualCorAr);
        assertFalse(actualIncorAr1);
        assertFalse(actualIncorAr2);
    }

    @Test
    public void test_validationId() {
        Controller controller = new Controller();
        String incorrectId = "ff";
        String correctId = "7";

        boolean actualCorId = controller.validationId(correctId);
        boolean actualIncorId = controller.validationId(incorrectId);

        assertTrue(actualCorId);
        assertFalse(actualIncorId);
    }
}
