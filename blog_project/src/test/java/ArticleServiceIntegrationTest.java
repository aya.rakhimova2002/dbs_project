import Model.Article;
import Service.ArticleService;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.jupiter.api.Order;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArticleServiceIntegrationTest {

    @AfterClass
    public static void after(){
        ArticleService service = new ArticleService();
        Article toDelete = service.findArticle(3);
        if (toDelete != null) {
            service.deleteArticle(toDelete);
        }
    }

    @Test
    @Order(1)
    public void test_getArticleById(){
        //arrange
        ArticleService service = new ArticleService();
        Article article = new Article(3, "Karlovy Vary", "Super!", "26.05.2021");

        //act
        service.saveArticle(article);
        Article expected = service.findArticle(3);

        //assert
        assertEquals(3, expected.getId());
        assertEquals("Karlovy Vary", expected.getTitle());
        assertEquals("Super!", expected.getText());
        assertEquals("26.05.2021", expected.getDate());
    }

    @Test
    @Order(2)
    public void test_updateAtricle(){
        //arrange
        ArticleService service = new ArticleService();
        Article actual = service.findArticle(3);
        actual.setDate("20.05.2021");
        actual.setText("Skvele!");

        //act
        Article expected = actual;

        //assert
        assertEquals(3, expected.getId());
        assertEquals("Karlovy Vary", expected.getTitle());
        assertEquals("Skvele!", expected.getText());
        assertEquals("20.05.2021", expected.getDate());
    }

    @Test
    @Order(3)
    public void test_deleteArticle(){
        ArticleService service = new ArticleService();
        Article article = new Article(3, "Karlovy Vary", "Super!", "26.05.2021");
        service.saveArticle(article);
        Article actual = service.findArticle(3);

        service.deleteArticle(actual);
        Article expected = service.findArticle(3);

        boolean status = false;
        if(expected == null){
            status = true;
        }else
            status = false;

        assertEquals(true, status);
    }
}
