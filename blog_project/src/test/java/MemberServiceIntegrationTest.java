import Model.Member;
import Service.MemberService;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.jupiter.api.Order;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class MemberServiceIntegrationTest {

    @AfterClass
    public static void after(){
        MemberService service = new MemberService();
        Member toDelete = service.findMember(10);
        service.deleteMember(toDelete);
    }

    @Test
    @Order(1)
    public void test_newMember_and_updateMember(){
        //arrange
        MemberService service = new MemberService();
        Member member = new Member(10, "Aiya", "Rakhimova", "aiya@gmail.com", "+420777000111", "fotograf");

        //act
        service.saveMember(member);

        Member actual = service.findMember(10);
        assertEquals(10, actual.getId());

        member.setRole("copywriter");
        member.setEmail("aiya_ra@gmail.com");
        service.updateMember(member);

        //assert
        assertEquals(10, member.getId());
        assertEquals("Aiya", member.getName());
        assertEquals("Rakhimova", member.getSurname());
        assertEquals("aiya_ra@gmail.com", member.getEmail());
        assertEquals("+420777000111", member.getPhone());
        assertEquals("copywriter", member.getRole());
    }

    @Test
    @Order(2)
    public void test_newMember_and_deleteMember(){
        //arrange
        MemberService service = new MemberService();
        Member member = new Member(11, "Selina", "Kadyrova", "selina@gmail.com", "+420777000222", "fotograf");

        //act
        service.saveMember(member);

        Member actual = service.findMember(11);
        assertEquals(11, actual.getId());

        List<Member> list = service.getAllMembers();
        service.deleteMember(member);
        List<Member> list_after_delete = service.getAllMembers();

        //assert
        assertEquals(list.size() - 1, list_after_delete.size());
    }

    @Test
    @Order(3)
    public void test_findAllMembers(){
        //arrange
        MemberService service = new MemberService();
        List<Member> list = service.getAllMembers();

        //assert
        assertEquals(2, list.size());
    }
}
