import DAO.MemberDao;
import Model.Article;
import Model.Create;
import Model.Member;
import Service.ArticleService;
import Service.CreateService;
import Service.MemberService;
import javafx.event.ActionEvent;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.regex.Matcher;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UnitTests {
    private Controller controller = Mockito.mock(Controller.class);
    private MemberService memberService = Mockito.mock(MemberService.class);
    private ArticleService articleService = Mockito.mock(ArticleService.class);
    private CreateService createService = Mockito.mock(CreateService.class);

    @Test
    public void testCheckPhone(){
        Controller controller = new Controller();
        String correctPhone = "420777345287";
        String incorrectPhone = "4207a77345f2g87";

        boolean actCorPhone = controller.checkPhone(correctPhone);
        boolean actIncorPhone = controller.checkPhone(incorrectPhone);

        assertTrue(actCorPhone);
        assertFalse(actIncorPhone);
    }

    @Test
    public void testCheckString(){
        Controller controller = new Controller();
        String correctString = "Cold";
        String incorrectString = "Cold123";

        boolean actCorString = controller.checkString(correctString);
        boolean actIncorString = controller.checkString(incorrectString);

        assertTrue(actCorString);
        assertFalse(actIncorString);
    }

    @Test
    public void testCheckEmail_incorrectData() {
        Controller controller = new Controller();
        String incorrectEmail1 = "incor@f..cz";
        String incorrectEmail2 = "test123@";
        String incorrectEmail3 = "test123456tset";

        boolean actIncorEm1 = controller.checkEmail(incorrectEmail1);
        boolean actIncorEm2 = controller.checkEmail(incorrectEmail2);
        boolean actIncorEm3 = controller.checkEmail(incorrectEmail3);

        assertFalse(actIncorEm1);
        assertFalse(actIncorEm2);
        assertFalse(actIncorEm3);
    }

    @Test
    public void testCheckEmail_correctData() {
        Controller controller = new Controller();
        String correctEmail = "anna44@gmail.com";

        boolean actCorEm = controller.checkEmail(correctEmail);

        assertTrue(actCorEm);
    }

    @Test
    public void checkDate_correctData_true() {
        Controller controller = new Controller();
        Article article = correctArticleData();

        boolean actCorDate = controller.checkDate(article);

        assertTrue(actCorDate);
    }

    @Test
    public void checkDate_incorrectData_false() {
        Controller controller = new Controller();
        Article article = incorrectArticleData();

        boolean actCorDate = controller.checkDate(article);

        assertFalse(actCorDate);
    }

    @Test
    public void testValidationMember_correctData() {
        Controller controller = new Controller();
        Member member = correctMemberData();

        Mockito.when(memberService.isMemberExist(member.getId())).thenReturn(true);
        boolean actCorMemInf = controller.validationMember(member);

        assertTrue(actCorMemInf);
    }

    @Test
    public void testValidationMember_incorrectData() {
        Controller controller = new Controller();
        Member member = incorrectMemberData();

        when(memberService.isMemberExist(member.getId())).thenReturn(true);
        boolean actIncorMemInf = controller.validationMember(member);

        assertFalse(actIncorMemInf);
    }

    @Test
    public void AddNewMemberToDB_incorrectData_null() {
        Member member = incorrectMemberData();
        String[] incorrectData = new String[]{
                "4",
                "An_na",
                "Morozova2",
                "anna44@@gmail.",
                "4207a77345f2g87",
                "editor"
        };

        when(controller.isEmpty(incorrectData)).thenReturn(true);
        when(memberService.findMember(member.getId())).thenReturn(null);
        when(controller.validationId(incorrectData[0])).thenReturn(true);
        boolean actVal = controller.validationMember(member);
        Member actMember = controller.addNewMemberToDB(member);

        assertFalse(actVal);
        assertNull(actMember);
    }


    @Test
    public void addNewRelation_DataAlreadyExistsInDB() {
        Create create = correctCreateData();
        Member member = correctMemberData();
        String[] correctData = new String[] {
                "15",
                "1"
        };

        when(controller.isEmpty(correctData)).thenReturn(true);
        when(memberService.findMember(Integer.parseInt(correctData[0]))).thenReturn(correctMemberData());
        when(articleService.findArticle(Integer.parseInt(correctData[1]))).thenReturn(correctArticleData());
        when(createService.findById(member.getId())).thenReturn(create);
        Create actCreate = controller.addNewRelationToDB(create);

        assertNull(actCreate);
    }

    @Test
    public void addNewArticleToDB_dataWithIncorrectDate_falseAndNull(){
        Article article = incorrectArticleData();
        String[] correctData = new String[] {
                "1",
                "Novy clanek",
                "Text noveho clanku",
                "2020-05-09"
        };

        when(controller.validationId(correctData[0])).thenReturn(true);
        when(controller.checkDate(article)).thenReturn(false);
        Article actArticle = controller.addNewArticleToDB(article);

        assertNull(actArticle);
    }



    public Member correctMemberData() {
        int id = 15;
        String name = "Anna";
        String surname = "Morozova";
        String email = "anna44@gmail.com";
        String phone = "420777345287";
        String role = "editor";
        Member member = new Member(id, name, surname, email, phone, role);
        return member;
    }

    public Member incorrectMemberData() {
        int id = 4;
        String name = "";
        String surname = "Morozova123";
        String email = "anna44@@gmail.";
        String phone = "4207a77345f2g87";
        String role = "editor";
        Member member = new Member(id, name, surname, email, phone, role);
        return member;
    }

    public Article correctArticleData() {
        int id = 1;
        String title = "Novy clanek";
        String text = "Text noveho clanku";
        String date = "19-05-2020";
        Article article = new Article(id, title, text, date);
        return article;
    }

    public Article incorrectArticleData() {
        int id = 1;
        String title = "Novy clanek";
        String text = "Text noveho clanku";
        String date = "2020-05-09";
        Article article = new Article(id, title, text, date);
        return article;
    }

    public Create correctCreateData() {
        int id_member = 15;
        int id_article = 1;
        Create create = new Create(id_member, id_article);
        return create;
    }

}
